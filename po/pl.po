# Polish translation for gnome-music.
# Copyright © 2012-2023 the gnome-music authors.
# This file is distributed under the same license as the gnome-music package.
# Piotr Drąg <piotrdrag@gmail.com>, 2012-2023.
# Paweł Żołnowski <pawel@zolnowski.name>, 2014-2015.
# Aviary.pl <community-poland@mozilla.org>, 2012-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-music\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-music/issues\n"
"POT-Creation-Date: 2023-07-04 07:49+0000\n"
"PO-Revision-Date: 2023-08-06 17:33+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: data/org.gnome.Music.appdata.xml.in.in:6
#: data/org.gnome.Music.desktop.in.in:3 gnomemusic/about.py:243
#: gnomemusic/application.py:61 gnomemusic/window.py:71
msgid "Music"
msgstr "Muzyka"

#: data/org.gnome.Music.appdata.xml.in.in:7
#: data/org.gnome.Music.desktop.in.in:5
msgid "Play and organize your music collection"
msgstr "Odtwarzanie i organizowanie kolekcji muzyki"

#: data/org.gnome.Music.appdata.xml.in.in:9
msgid "An easy and pleasant way to play your music."
msgstr "Łatwy i przyjemny sposób na odtwarzanie muzyki."

#: data/org.gnome.Music.appdata.xml.in.in:12
msgid ""
"Find tracks in your local collection, use automatically generated playlists "
"or curate a fresh one."
msgstr ""
"Znajduj utwory w swojej lokalnej kolekcji, korzystaj z automatycznie "
"tworzonych list odtwarzania lub utwórz własne."

#: data/org.gnome.Music.appdata.xml.in.in:182
msgid "The GNOME Music developers"
msgstr "Programiści odtwarzacza muzyki GNOME"

#: data/org.gnome.Music.desktop.in.in:4
msgid "Music Player"
msgstr "Odtwarzacz muzyki"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Music.desktop.in.in:13
msgid "Music;Player;"
msgstr ""
"Muzyka;Odtwarzacz;Dźwięk;Audio;Piosenka;Utwór;MP3;Ogg;Vorbis;FLAC;CD;Płyta;"
"Playlista;Lista;odtwarzania;Last.fm;Lastfm;Jamendo;Magnatune;UPnP;DLNA;MPRIS;"
"ReplayGain;"

#: data/org.gnome.Music.gschema.xml:12
msgid "Window size"
msgstr "Rozmiar okna"

#: data/org.gnome.Music.gschema.xml:13
msgid "Window size (width and height)."
msgstr "Rozmiar okna (szerokość i wysokość)."

#: data/org.gnome.Music.gschema.xml:17
msgid "Window maximized"
msgstr "Maksymalizacja okna"

#: data/org.gnome.Music.gschema.xml:18
msgid "Window maximized state."
msgstr "Stan maksymalizacji okna."

#: data/org.gnome.Music.gschema.xml:22
msgid "Playback repeat mode"
msgstr "Tryb powtarzania odtwarzania"

#. Translators: Don't translate allowed values, just the description in the brackets
#: data/org.gnome.Music.gschema.xml:24
msgid ""
"Value identifies whether to repeat or randomize playback through the "
"collection. Allowed values are: “none” (repeat and shuffle are off), "
"“song” (repeat current song), “all” (repeat playlist, no shuffle), "
"“shuffle” (shuffle playlist, presumes repeat all)."
msgstr ""
"Wartość określa, czy powtarzać lub losować kolejność odtwarzania kolekcji. "
"Dozwolone wartości: „none” (powtarzanie i odtwarzanie losowe wyłączone), "
"„song” (powtarzanie bieżącego utworu), „all” (powtarzanie listy odtwarzania, "
"bez odtwarzania losowego), „shuffle” (odtwarzanie losowe i powtarzanie listy "
"odtwarzania)."

#: data/org.gnome.Music.gschema.xml:28
msgid "Enable ReplayGain"
msgstr "Włączenie ReplayGain"

#: data/org.gnome.Music.gschema.xml:29
msgid "Enables or disables ReplayGain for albums"
msgstr "Normalizacja poziomu głośności ReplayGain dla albumów"

#: data/org.gnome.Music.gschema.xml:33
msgid "Inhibit system suspend"
msgstr "Wstrzymanie uśpienia komputera"

#: data/org.gnome.Music.gschema.xml:34
msgid "Enables or disables inhibiting system suspend while playing music"
msgstr "Wstrzymuje uśpienie komputera podczas odtwarzania muzyki"

#: data/org.gnome.Music.gschema.xml:38
msgid "Report music history to Last.fm"
msgstr "Zgłaszanie historii odtwarzanej muzyki w serwisie Last.fm"

#: data/org.gnome.Music.gschema.xml:39
msgid ""
"Enables or disables sending scrobbles and the “currently playing” info to "
"Last.fm."
msgstr ""
"Wysyła odsłuchane utwory i informacje o obecnie odtwarzanym do serwisu Last."
"fm."

#: data/ui/AlbumWidget.ui:84 data/ui/PlayerToolbar.ui:87
#: data/ui/PlaylistControls.ui:101 gnomemusic/widgets/playertoolbar.py:170
msgid "Play"
msgstr "Odtwarza"

#: data/ui/AlbumWidget.ui:130 data/ui/PlaylistControls.ui:5
#: data/ui/SongWidgetMenu.ui:11
msgid "_Play"
msgstr "O_dtwórz"

#: data/ui/AlbumWidget.ui:134
msgid "Add to _Favorite Songs"
msgstr "Dodaj do _ulubionych utworów"

#: data/ui/AlbumWidget.ui:138 data/ui/SongWidgetMenu.ui:15
msgid "_Add to Playlist…"
msgstr "_Dodaj do listy odtwarzania…"

#: data/ui/AppMenu.ui:23 data/ui/LastfmDialog.ui:8
msgid "Last.fm Account"
msgstr "Konto Last.fm"

#: data/ui/AppMenu.ui:36
msgid "Report Music Listening"
msgstr "Zgłaszanie odsłuchanych utworów"

#: data/ui/AppMenu.ui:58
msgid "_Keyboard Shortcuts"
msgstr "_Skróty klawiszowe"

#: data/ui/AppMenu.ui:68
msgid "_Help"
msgstr "Pomo_c"

#: data/ui/AppMenu.ui:78
msgid "_About Music"
msgstr "_O programie"

#: data/ui/EmptyView.ui:30
msgid "Welcome to Music"
msgstr "Witamy w odtwarzaczu muzyki"

#: data/ui/HeaderBar.ui:17
msgid "Menu"
msgstr "Menu"

#: data/ui/HeaderBar.ui:25 data/ui/SearchHeaderBar.ui:15
msgid "Select"
msgstr "Zaznacza"

#: data/ui/HeaderBar.ui:31 data/ui/PlaylistDialog.ui:213
#: data/ui/SearchHeaderBar.ui:20
msgid "_Cancel"
msgstr "_Anuluj"

#: data/ui/HeaderBar.ui:43 data/ui/SearchHeaderBar.ui:32
msgid "Search"
msgstr "Wyszukuje"

#: data/ui/HeaderBar.ui:51
msgid "Back"
msgstr "Wstecz"

#: data/ui/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Ogólne"

#: data/ui/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Close window"
msgstr "Zamknięcie okna"

#: data/ui/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Search"
msgstr "Wyszukiwanie"

#: data/ui/help-overlay.ui:26
msgctxt "shortcut window"
msgid "Help"
msgstr "Pomoc"

#: data/ui/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Shortcuts"
msgstr "Skróty"

#: data/ui/help-overlay.ui:40
msgctxt "shortcut window"
msgid "Playback"
msgstr "Odtwarzanie"

#: data/ui/help-overlay.ui:43
msgctxt "shortcut window"
msgid "Play/Pause"
msgstr "Odtwarzanie/wstrzymanie"

#: data/ui/help-overlay.ui:49
msgctxt "shortcut window"
msgid "Next song"
msgstr "Następny utwór"

#: data/ui/help-overlay.ui:55
msgctxt "shortcut window"
msgid "Previous song"
msgstr "Poprzedni utwór"

#: data/ui/help-overlay.ui:61
msgctxt "shortcut window"
msgid "Toggle repeat"
msgstr "Przełączenie powtarzania"

#: data/ui/help-overlay.ui:67
msgctxt "shortcut window"
msgid "Toggle shuffle"
msgstr "Przełączenie losowania"

#: data/ui/help-overlay.ui:75
msgctxt "shortcut window"
msgid "Navigation"
msgstr "Nawigacja"

#: data/ui/help-overlay.ui:78
msgctxt "shortcut window"
msgid "Go to Albums"
msgstr "Przejście do albumów"

#: data/ui/help-overlay.ui:84
msgctxt "shortcut window"
msgid "Go to Artists"
msgstr "Przejście do wykonawców"

#: data/ui/help-overlay.ui:90
msgctxt "shortcut window"
msgid "Go to Songs"
msgstr "Przejście do utworów"

#: data/ui/help-overlay.ui:96
msgctxt "shortcut window"
msgid "Go to Playlists"
msgstr "Przejście do list odtwarzania"

#: data/ui/help-overlay.ui:102
msgctxt "shortcut window"
msgid "Go back"
msgstr "Wstecz"

#: data/ui/LastfmDialog.ui:21
msgid ""
"Last.fm is a music discovery service that gives you personalised "
"recommendations based on the music you listen to."
msgstr ""
"Last.fm to serwis do odkrywania muzyki zapewniający spersonalizowane "
"polecenia na podstawie odsłuchanych utworów."

#: data/ui/LastfmDialog.ui:31 gnomemusic/widgets/lastfmdialog.py:57
msgid "Music Reporting Not Setup"
msgstr "Nie skonfigurowano zgłaszania muzyki"

#: data/ui/LastfmDialog.ui:44 gnomemusic/widgets/lastfmdialog.py:60
msgid "Login to your Last.fm account to report your music listening."
msgstr "Zgłaszanie odsłuchanych utworów wymaga zalogowania na konto Last.fm."

#: data/ui/LastfmDialog.ui:55 gnomemusic/widgets/lastfmdialog.py:58
msgid "Login"
msgstr "Zaloguj się"

#: data/ui/PlayerToolbar.ui:74
msgid "Previous"
msgstr "Poprzedni"

#: data/ui/PlayerToolbar.ui:106
msgid "Next"
msgstr "Następny"

#: data/ui/PlaylistControls.ui:9
msgid "_Delete"
msgstr "_Usuń"

#: data/ui/PlaylistControls.ui:13
msgid "_Rename…"
msgstr "_Zmień nazwę…"

#: data/ui/PlaylistControls.ui:28
msgid "Playlist Name"
msgstr "Nazwa listy odtwarzania"

#: data/ui/PlaylistControls.ui:62
msgid "_Done"
msgstr "_Gotowe"

#: data/ui/PlaylistDialog.ui:52
msgid "Enter a name for your first playlist"
msgstr "Nazwa pierwszej listy odtwarzania"

#: data/ui/PlaylistDialog.ui:77
msgid "C_reate"
msgstr "_Utwórz"

#: data/ui/PlaylistDialog.ui:146
msgid "New Playlist…"
msgstr "Nowa lista odtwarzania…"

#: data/ui/PlaylistDialog.ui:161
msgid "Add"
msgstr "Dodaj"

#: data/ui/PlaylistDialog.ui:202
msgid "Add to Playlist"
msgstr "Dodanie do listy odtwarzania"

#: data/ui/PlaylistDialog.ui:222
msgid "_Add"
msgstr "_Dodaj"

#: data/ui/SearchView.ui:33 gnomemusic/views/artistsview.py:51
msgid "Artists"
msgstr "Wykonawcy"

#: data/ui/SearchView.ui:42 data/ui/SearchView.ui:84
msgid "View All"
msgstr "Wyświetl wszystko"

#: data/ui/SearchView.ui:75 gnomemusic/views/albumsview.py:56
msgid "Albums"
msgstr "Albumy"

#: data/ui/SearchView.ui:117 gnomemusic/views/songsview.py:52
msgid "Songs"
msgstr "Utwory"

#: data/ui/SelectionBarMenuButton.ui:7
msgid "Select All"
msgstr "Zaznacz wszystko"

#: data/ui/SelectionBarMenuButton.ui:11
msgid "Select None"
msgstr "Odznacz wszystko"

#: data/ui/SelectionBarMenuButton.ui:19 gnomemusic/widgets/headerbar.py:69
msgid "Click on items to select them"
msgstr "Kliknięcie elementu zaznacza go"

#: data/ui/SelectionToolbar.ui:8
msgid "_Add to Playlist"
msgstr "_Dodaj do listy odtwarzania"

#: data/ui/SongWidgetMenu.ui:19
msgid "_Remove from Playlist"
msgstr "_Usuń z listy odtwarzania"

#: gnomemusic/about.py:245
msgid "The GNOME Project"
msgstr "Projekt GNOME"

#: gnomemusic/about.py:249
msgid "translator-credits"
msgstr ""
"Piotr Drąg <piotrdrag@gmail.com>, 2012-2023\n"
"Paweł Żołnowski <pawel@zolnowski.name>, 2014-2015\n"
"Aviary.pl <community-poland@mozilla.org>, 2012-2023"

#: gnomemusic/about.py:253
msgid "Copyright The GNOME Music Developers"
msgstr "Copyright © programiści odtwarzacza muzyki GNOME"

#: gnomemusic/about.py:256
msgid "Translated by"
msgstr "Tłumaczenie"

#. TRANSLATORS: this is a playlist name
#: gnomemusic/grilowrappers/grltrackerplaylists.py:894
msgid "Most Played"
msgstr "Najczęściej odtwarzane"

#. TRANSLATORS: this is a playlist name
#: gnomemusic/grilowrappers/grltrackerplaylists.py:948
msgid "Never Played"
msgstr "Nigdy nieodtworzone"

#. TRANSLATORS: this is a playlist name
#: gnomemusic/grilowrappers/grltrackerplaylists.py:1001
msgid "Recently Played"
msgstr "Ostatnio odtwarzane"

#. TRANSLATORS: this is a playlist name
#: gnomemusic/grilowrappers/grltrackerplaylists.py:1064
msgid "Recently Added"
msgstr "Ostatnio dodane"

#. TRANSLATORS: this is a playlist name
#: gnomemusic/grilowrappers/grltrackerplaylists.py:1127
msgid "Favorite Songs"
msgstr "Ulubione utwory"

#: gnomemusic/gstplayer.py:408
msgid "Unable to play the file"
msgstr "Nie można odtworzyć pliku"

#: gnomemusic/gstplayer.py:414
msgid "_Find in {}"
msgstr "_Znajdź w „{}”"

#. TRANSLATORS: separator for two codecs
#: gnomemusic/gstplayer.py:424
msgid " and "
msgstr " i "

#. TRANSLATORS: separator for a list of codecs
#: gnomemusic/gstplayer.py:427
msgid ", "
msgstr ", "

#: gnomemusic/gstplayer.py:429
msgid "{} is required to play the file, but is not installed."
msgid_plural "{} are required to play the file, but are not installed."
msgstr[0] "{} jest wymagane do odtworzenia pliku, ale nie jest zainstalowane."
msgstr[1] "{} są wymagane do odtworzenia pliku, ale nie są zainstalowane."
msgstr[2] "{} są wymagane do odtworzenia pliku, ale nie są zainstalowane."

#: gnomemusic/inhibitsuspend.py:63
msgid "Playing music"
msgstr "Odtwarzanie muzyki"

#. Translators: "shuffle" causes tracks to play in random order.
#: gnomemusic/player.py:44
msgid "Shuffle/Repeat Off"
msgstr "Wyłączenie losowania/powtarzania"

#: gnomemusic/player.py:45
msgid "Repeat Song"
msgstr "Powtarzanie utworu"

#: gnomemusic/player.py:46
msgid "Repeat All"
msgstr "Powtarzanie wszystkich"

#: gnomemusic/player.py:47
msgid "Shuffle"
msgstr "Odtwarzanie losowe"

#: gnomemusic/playlisttoast.py:55
msgid "Playlist {} removed"
msgstr "Usunięto listę odtwarzania „{}”"

#: gnomemusic/playlisttoast.py:56 gnomemusic/songtoast.py:65
msgid "Undo"
msgstr "Cofnij"

#: gnomemusic/songtoast.py:64
msgid "{} removed from {}"
msgstr "Usunięto utwór „{}” z listy „{}”"

#: gnomemusic/utils.py:92
msgid "Unknown album"
msgstr "Nieznany album"

#: gnomemusic/utils.py:113
msgid "Unknown Artist"
msgstr "Nieznany wykonawca"

#: gnomemusic/views/emptyview.py:65
msgid "Your XDG Music directory is not set."
msgstr "Katalog „Muzyka” standardu XDG nie jest ustawiony."

#: gnomemusic/views/emptyview.py:72
msgid "Music Folder"
msgstr "katalogu z muzyką"

#. TRANSLATORS: This is a label to display a link to open user's music
#. folder. {} will be replaced with the translated text 'Music folder'
#: gnomemusic/views/emptyview.py:76
msgid "The contents of your {} will appear here."
msgstr "W tym miejscu będzie znajdować się zawartość {}."

#: gnomemusic/views/emptyview.py:124
msgid "No Music Found"
msgstr "Nie odnaleziono plików muzycznych"

#: gnomemusic/views/emptyview.py:125
msgid "Try a Different Search"
msgstr "Proszę spróbować innych słów"

#: gnomemusic/views/emptyview.py:129
msgid "GNOME Music could not connect to Tracker."
msgstr "Odtwarzacz muzyki GNOME nie może połączyć się z usługą Tracker."

#: gnomemusic/views/emptyview.py:131
msgid "Your music files cannot be indexed without Tracker running."
msgstr "Bez działającej usługi Tracker nie można wykryć plików z muzyką."

#: gnomemusic/views/emptyview.py:137
msgid "Your system Tracker version seems outdated."
msgstr "Wersja usługi Tracker zainstalowana na komputerze jest przestarzała."

#: gnomemusic/views/emptyview.py:139
msgid "Music needs Tracker version 3.0.0 or higher."
msgstr "Odtwarzacz muzyki wymaga wersji 3.0.0 lub wyższej."

#: gnomemusic/views/playlistsview.py:45
msgid "Playlists"
msgstr "Listy odtwarzania"

#: gnomemusic/views/searchview.py:304
msgid "Artists Results"
msgstr "Wyniki wyszukiwania wśród wykonawców"

#: gnomemusic/views/searchview.py:318
msgid "Albums Results"
msgstr "Wyniki wyszukiwania wśród albumów"

#: gnomemusic/widgets/albumwidget.py:245
msgid "{} minute"
msgid_plural "{} minutes"
msgstr[0] "{} minuta"
msgstr[1] "{} minuty"
msgstr[2] "{} minut"

#: gnomemusic/widgets/discbox.py:75
msgid "Disc {}"
msgstr "{}. płyta"

#: gnomemusic/widgets/headerbar.py:66
msgid "Selected {} song"
msgid_plural "Selected {} songs"
msgstr[0] "Zaznaczono {} utwór"
msgstr[1] "Zaznaczono {} utwory"
msgstr[2] "Zaznaczono {} utworów"

#: gnomemusic/widgets/lastfmdialog.py:64
msgid "Your music listening is reported to Last.fm."
msgstr "Odsłuchane utwory są zgłaszane w serwisie Last.fm."

#: gnomemusic/widgets/lastfmdialog.py:66
msgid "Your music listening is not reported to Last.fm."
msgstr "Odsłuchane utwory nie są zgłaszane w serwisie Last.fm."

#. TRANSLATORS: displays the username of the Last.fm account
#: gnomemusic/widgets/lastfmdialog.py:70
msgid "Logged in as {}"
msgstr "Zalogowano jako {}"

#: gnomemusic/widgets/lastfmdialog.py:71
msgid "Configure"
msgstr "Skonfiguruj"

#: gnomemusic/widgets/playertoolbar.py:167
msgid "Pause"
msgstr "Wstrzymuje"

#: gnomemusic/widgets/playlistcontrols.py:130
msgid "{} Song"
msgid_plural "{} Songs"
msgstr[0] "{} utwór"
msgstr[1] "{} utwory"
msgstr[2] "{} utworów"

#: gnomemusic/widgets/searchheaderbar.py:66
msgid "Search songs, artists, albums and playlists"
msgstr "Wyszukiwanie utworów, wykonawców, albumów i list odtwarzania"
